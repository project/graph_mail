<?php

namespace Drupal\graph_mail;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Queue\QueueFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\TransferException;
use Microsoft\Graph\Graph;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Mime\Address;

/**
 * Sends mail via the Microsoft Graph API.
 *
 * This service is used from the mail and from the QueueWorker plugins.
 */
class MailHelper {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * Constructs a new \Drupal\graph_mail\MailHelper object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    TimeInterface $time,
    QueueFactory $queue_factory,
  ) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->time = $time;
    $this->queueFactory = $queue_factory;
  }

  /**
   * Retrieves a configuration object.
   *
   * @param string $name
   *   The name of the configuration object to retrieve, which corresponds to a
   *   configuration file.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   A configuration object.
   */
  public function config(string $name): ImmutableConfig {
    return $this->configFactory->get($name);
  }

  /**
   * Requests a token used to send emails.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response object.
   *
   * @throws \GuzzleHttp\Exception\TransferException
   *   An error happened when requesting the token.
   */
  protected function requestToken(): ResponseInterface {
    $config = $this->config('graph_mail.settings');

    return $this->httpClient->request("POST",
      'https://login.microsoftonline.com/' . $config->get('tenant_id') . '/oauth2/v2.0/token', [
        'form_params' => [
          'client_id' => $config->get('client_id'),
          'client_secret' => $config->get('client_secret'),
          'scope' => 'https://graph.microsoft.com/.default',
          'grant_type' => 'client_credentials',
        ],
      ]
    );
  }

  /**
   * Initializes the \Microsoft\Graph\Graph object used to send emails.
   *
   * @return \Microsoft\Graph\Graph
   *   The object used to send emails via the Microsoft Graph API.
   *
   * @throws \JsonException
   *   An error happened when decoding the token response.
   * @throws \Drupal\graph_mail\GraphMailInitException
   *   An error happened when initializing the Microsoft Graph API.
   * @throws \GuzzleHttp\Exception\TransferException
   *   An error happened when requesting the token.
   */
  protected function initGraph(): Graph {
    $config = $this->config('graph_mail.settings');
    /** @var \Psr\Http\Message\ResponseInterface $response */
    $response = $this->requestToken();

    if ($response->getStatusCode() != 200) {
      throw new GraphMailInitException('An error happened when requesting the token.', $response->getStatusCode());
    }

    $obj = json_decode($response->getBody()->getContents(), flags: JSON_THROW_ON_ERROR);

    if (!$obj || !isset($obj->access_token)) {
      throw new GraphMailInitException('The response did not return the expected JSON data.');
    }

    /** @var \Microsoft\Graph\Graph $graph */
    $graph = (new Graph())
      ->setAccessToken($obj->access_token);

    if ($version = $config->get('version')) {
      // Correct the version, in the case the update hook has not been yet
      // invoked.
      $graph->setApiVersion($version === '1.0' ? 'v1.0' : $version);
    }

    return $graph;
  }

  /**
   * Creates an array of addresses from a string containing email addresses.
   *
   * @param string $addresses
   *   The string containing email addresses, separated by commas.
   *
   * @return array
   *   An array of array containing the following elements:
   *     - 'address': The email address
   *     - 'name': The name associated to the email address
   */
  protected function splitAddresses(string $addresses): array {
    $return = [];

    foreach (explode(',', $addresses) as $address) {
      $return[] = ['emailAddress' => $this->parseEmailAddress($address)];
    }

    return $return;
  }

  /**
   * Parses the email address to comply to the emailAddress resource type.
   *
   * @param string $email
   *   The email address to parse.
   *
   * @return array
   *   An array containing the parsed email address.
   *
   * @see https://learn.microsoft.com/en-us/graph/api/resources/emailaddress?view=graph-rest-1.0
   */
  protected function parseEmailAddress(string $email): array {
    $address = Address::create(trim($email));

    return [
      'address' => $address->getAddress(),
      'name' => $address->getName(),
    ];
  }

  /**
   * Initializes the mail body from the message array passed to hook_mail().
   *
   * @param array $message
   *   The message array passed to the email plugin.
   *
   * @return array
   *   The mail body array used from the Microsoft Graph API.
   *
   * @see hook_mail()
   */
  public function initMailBody(array $message): array {
    $config = $this->config('graph_mail.settings');
    $content = '';

    if (empty($message['body'])) {
      $message['body'] = $message['params']['message'];
    }

    if (!empty($message['body'])) {
      if (is_string($message['body']) || is_a($message['body'], MarkupInterface::class)) {
        $content = (string) $message['body'];
      }
      elseif (is_array($message['body'])) {
        $content = reset($message['body']);
      }
    }

    if (empty($message['subject'])) {
      $message['subject'] = $message['params']['subject'];
    }

    $mail_body = [
      'message' => [
        'subject' => $message['subject'],
        'body' => [
          'contentType' => 'HTML',
          'content' => $content,
        ],
        'from' => [
          'emailAddress' => [
            'address' => $config->get('default_mail') ?? $this->config('system.site')->get('mail'),
          ],
        ],
        'toRecipients' => [],
        'ccRecipients' => [],
        'bccRecipients' => [],
        'replyTo' => [],
      ],
      'saveToSentItems' => $config->get('save_to_sent_items') ? 'true' : 'false',
    ];

    $mail_body['message']['toRecipients'] = $this->splitAddresses($message['to']);

    if (!empty($message['headers'])) {
      $headers = array_change_key_case($message['headers']);

      if (!empty($headers['bcc'])) {
        $mail_body['message']['bccRecipients'] = $this->splitAddresses($headers['bcc']);
      }

      if (!empty($headers['cc'])) {
        $mail_body['message']['ccRecipients'] = $this->splitAddresses($headers['cc']);
      }

      if (!empty($headers['reply-to'])) {
        $mail_body['message']['replyTo'] = $this->splitAddresses($headers['reply-to']);
      }
    }

    if (isset($message['params']['attachments'])) {
      $files = $message['params']['attachments'];
      foreach ($files as $file) {
        $mail_body['message']['attachments'][] = [
          '@odata.type' => '#microsoft.graph.fileAttachment',
          'name' => $file['filename'],
          'contentBytes' => base64_encode($file['filecontent']),
        ];
      }
    }

    return $mail_body;
  }

  /**
   * Sends emails using the Microsoft Graph API.
   *
   * @param array $mail_body
   *   The array for the message to send.
   *
   * @throws \JsonException
   *    An error happened when decoding the token response.
   * @throws \Drupal\graph_mail\GraphMailInitException
   *    An error happened when initializing the Microsoft Graph API.
   * @throws \GuzzleHttp\Exception\TransferException
   *    An error happened when requesting the token.
   *
   * @see \Drupal\graph_mail\MailHelper::initGraph()
   * @see \Drupal\graph_mail\MailHelper::initMailBody()
   */
  public function send(array $mail_body): void {
    $config = $this->config('graph_mail.settings');
    $graph = $this->initGraph();

    $graph->createRequest('POST', '/users/' . $config->get('user_id') . '/sendmail')
      ->attachBody($mail_body)
      ->execute();
  }

  /**
   * Handles the exception thrown by Drupal\graph_mail\MailHelper::send().
   *
   * @param \Exception $e
   *   The exception thrown by Drupal\graph_mail\MailHelper::send().
   *
   * @return int
   *   The number of seconds to delay before retrying, in the case the Microsoft
   *   Graph API asked to retry, or 0 in the case the exception was thrown for
   *   any other error.
   */
  public function handleException(\Exception $e): int {
    $delay = 0;

    if ($e instanceof TransferException) {
      /** @var Psr\Http\Message\ResponseInterface $response */
      $response = $e->getResponse();

      if ($response->getStatusCode() === 429) {
        $delay = 600;

        if ($response->hasHeader('Retry-After')) {
          if (($retry = $response->getHeader('Retry-After')[0]) > 0) {
            $delay = $retry;
          }
        }
      }
    }

    return $delay;
  }

  /**
   * Queues the message to send it $delay seconds later.
   *
   * @param array $mail_body
   *   The array for the message to send.
   * @param int $delay
   *   The number of seconds to delay before retrying.
   */
  public function queueMessage(array $mail_body, int $delay) {
    $queue = $this->queueFactory->get('graph_mail_retry_queue');
    $message_info = new \stdClass();

    $message_info->body = $mail_body;
    $message_info->retry = $this->time->getCurrentTime() + $delay;

    $queue->createItem($message_info);
  }

}
