<?php

namespace Drupal\graph_mail\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graph_mail\MailHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends emails that previously failed to be sent.
 */
#[QueueWorker(
  id: 'graph_mail_retry_queue',
  title: new TranslatableMarkup('Graph Mail Retry Queue'),
  cron: ['time' => 30]
)]
class RetryQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The mail helper service.
   *
   * @var \Drupal\graph_mail\MailHelper
   */
  protected MailHelper $mailHelper;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Constructs a new \Drupal\graph_mail\Plugin\QueueWorker\RetryQueue object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\graph_mail\MailHelper $mail_helper
   *   The mail helper service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    MailHelper $mail_helper,
    TimeInterface $time,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mailHelper = $mail_helper;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('graph_mail.helper'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $current_time = $this->time->getCurrentTime();

    if (!empty($data->retry)) {
      if ($current_time > $data->retry) {
        try {
          $this->mailHelper->send($data->body);
        }
        catch (\Exception $e) {
          if ($delay = $this->mailHelper->handleException($e)) {
            $data->retry = $current_time + $delay;
            // If the exception thrown by the Microsoft Graph API asked to
            // retry, requeue the message after $delay seconds. For this, it is
            // sufficient to throw the Drupal\Core\Queue\DelayedRequeueException
            // exception.
            throw new DelayedRequeueException($delay);
          }
        }
      }
      else {
        // The queued message was not ready to be sent; requeue it.
        throw new DelayedRequeueException($data->retry - $current_time);
      }
    }
  }

}
