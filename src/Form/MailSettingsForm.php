<?php

namespace Drupal\graph_mail\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures the Microsoft Graph API Mail plugin.
 */
class MailSettingsForm extends ConfigFormBase {

  // phpcs:disable Generic.Files.LineLength.TooLong

  /**
   * Constructs a new Drupal\graph_mail\Form\MailSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param ?\Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationInterface $string_translation, ?TypedConfigManagerInterface $typed_config_manager = NULL) {
    if (!$typed_config_manager instanceof TypedConfigManagerInterface) {
      $type = get_debug_type($typed_config_manager);
      $typed_config_manager = \Drupal::getContainer()->get('config.typed');

      @trigger_error("Passing {$type} to the \$typed_config_manager parameter of MailSettingsForm::__construct() is deprecated in graph_mail:2.0.0 and must be an instance of \\Drupal\\Core\\Config\\TypedConfigManagerInterface in graph_mail:3.0.0. See https://www.drupal.org/node/3404140", E_USER_DEPRECATED);
    }

    parent::__construct($config_factory, $typed_config_manager);
    $this->setStringTranslation($string_translation);
  }

  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('string_translation'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'graph_mail.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['graph_mail.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('graph_mail.settings');
    $form = parent::buildForm($form, $form_state);

    $form['tenant_id'] = [
      '#title' => $this->t('Tenant ID'),
      '#description' => $this->t(
        'The directory tenant from which you want to request permission. The value must be in GUID format.'
      ),
      '#type' => 'textfield',
      '#default_value' => $config->get('tenant_id'),
      '#required' => TRUE,
    ];

    $form['client_id'] = [
      '#title' => $this->t('Client ID'),
      '#description' => $this->t(
        'The application ID the <a href=":registration_portal">Azure portal</a> assigned to your application.',
        [':registration_portal' => 'https://portal.azure.com/']
      ),
      '#type' => 'textfield',
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['client_secret'] = [
      '#title' => $this->t('Client secret'),
      '#description' => $this->t(
        'The client secret that you generated for your application in the registration portal, URL encoded.'
      ),
      '#type' => 'textfield',
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];

    $form['user_id'] = [
      '#title' => $this->t('User ID'),
      '#type' => 'textfield',
      '#default_value' => $config->get('user_id'),
      '#required' => TRUE,
    ];

    // Correct the version, in the case the update hook has not been yet
    // invoked.
    if (($version = $config->get('version')) === '1.0') {
      $version = 'v1.0';
    }

    $form['version'] = [
      '#title' => $this->t('API version'),
      '#description' => $this->t(
        'The version of the Graph API. Currently, only <em>v1.0</em> and <em>beta</em> are supported.'
      ),
      '#type' => 'textfield',
      '#default_value' => $version,
    ];

    $form['default_mail'] = [
      '#title' => $this->t('Default mail'),
      '#description' => $this->t('The mail used to send emails. Leave it empty to use the site mail.'),
      '#type' => 'textfield',
      '#default_value' => $config->get('default_mail'),
    ];

    $form['save_to_sent_items'] = [
      '#title' => $this->t('Save to sent items'),
      '#description' => $this->t('Select it to save the sent items.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('save_to_sent_items') ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $this->config('graph_mail.settings')
      ->set('tenant_id', $form_state->getValue('tenant_id'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('user_id', $form_state->getValue('user_id'))
      ->set('version', $form_state->getValue('version'))
      ->set('default_mail', $form_state->getValue('default_mail'))
      ->set('save_to_sent_items', $form_state->getValue('save_to_sent_items'))
      ->save();
  }

}
