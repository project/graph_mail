# Graph Mail

This module allows to send mail using the Microsoft Graph API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/graph_mail).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/graph_mail).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core. It requires the
[Microsoft Graph SDK for PHP](https://packagist.org/packages/microsoft/microsoft-graph).


## Recommended modules

[Mail System](https://www.drupal.org/project/mailsystem): It provides an
administrative UI and developers API for managing the used mail backend/plugin.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Follow what reported in
   [Microsoft Graph API (Send mail)](https://github.com/microsoftgraph/msgraph-sdk-php/files/7007074/Microsoft.Graph.API.PHP.Setup.pdf)
   to set up an application for emails
2. Go to /admin/config/services/graph_mail and enter the data obtained from
   Microsoft as per the first point
3. If the _Mail System_ module has been installed, go to
   admin/config/system/mailsystem and select _Graph Mail_ as default email
   sender


## Maintainers

- [Alberto Paderno](https://www.drupal.org/u/apaderno)
- [Bram Driesen](https://www.drupal.org/u/bramdriesen)
- [Edvinas edckinis](https://www.drupal.org/u/edcka)
