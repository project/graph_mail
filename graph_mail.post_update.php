<?php

/**
 * @file
 * Post update functions for the Graph Mail module.
 */

/**
 * Rebuild cache to ensure the service changes are read.
 */
function graph_mail_post_update_service_arguments_change() {
  // Empty update to cause a cache rebuilt so that the service changes are read.
}

/**
 * Rebuild cache to ensure the plugin changes are read.
 */
function graph_mail_post_update_plugin_definition_change() {
  // Empty update to cause a cache rebuilt so that the plugin changes are read.
}

/**
 * Rebuild cache to ensure the changes to the settings form class are read.
 */
function graph_mail_post_update_settings_form_definition_change() {
  // Empty update to cause a cache rebuilt so that the form changes are read.
}
