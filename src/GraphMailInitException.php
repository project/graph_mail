<?php

namespace Drupal\graph_mail;

/**
 * The exception thrown for init errors.
 */
class GraphMailInitException extends \RuntimeException {}
