<?php

namespace Drupal\graph_mail\Plugin\Mail;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Mail\Attribute\Mail;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Error;
use Drupal\graph_mail\MailHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Microsoft Graph API Mail plugin.
 */
#[Mail(
  id: 'graphmail',
  label: new TranslatableMarkup('Microsoft Graph API Mail'),
  description: new TranslatableMarkup("Sends messages via the Microsoft Graph API."),
)]
class GraphMail implements MailInterface, ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  /**
   * The mail helper service.
   *
   * @var \Drupal\graph_mail\MailHelper
   */
  protected MailHelper $mailHelper;

  /**
   * Constructs a new \Drupal\graph_mail\Plugin\Mail\GraphMail object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\graph_mail\MailHelper $mail_helper
   *   The mail helper service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
    MailHelper $mail_helper,
  ) {
    // Ignore the warning reported from the Generic.Files.LineLength rule as
    // the triggered error message is expected to be a literal string.
    // phpcs:disable Generic.Files.LineLength
    if ($mail_helper instanceof ConfigFactoryInterface) {
      @trigger_error('Calling MailHelper::__construct() with the $config_factory argument is deprecated in graph_mail:1.0.0-alpha1 and will be removed in graph_mail:2.0.0. See https://www.drupal.org/project/graph_mail/issues/3392607', E_USER_DEPRECATED);
      $mail_helper = \Drupal::service('graph_mail.helper');
    }

    // phpcs:enable
    $this->setLoggerFactory($logger_factory);
    $this->mailHelper = $mail_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('graph_mail.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    // Join the body array into one string.
    $message['body'] = implode("\n\n", $message['body']);
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    $mail_body = $this->mailHelper->initMailBody($message);

    try {
      $this->mailHelper->send($mail_body);
    }
    catch (\Exception $e) {
      Error::logException($this->getLogger('graph_mail'), $e);

      if ($delay = $this->mailHelper->handleException($e)) {
        $this->mailHelper->queueMessage($mail_body, $delay);
      }

      return FALSE;
    }

    return TRUE;
  }

}
